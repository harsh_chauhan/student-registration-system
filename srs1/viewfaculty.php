<?php

$dbhost = "localhost";
 $dbuser = "root";
 $dbpass = "";
 $db = "student";


 $conn = new mysqli($dbhost, $dbuser, $dbpass,$db) or die("Connect failed: %s\n". $conn -> error);

?>

<head>
<title>Faculty </title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<link rel="stylesheet" href="style3.css">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

</head>
<body style="background-image:url()">
  <div class="menu-bar">
<ul>
<li class="active"> <a href="homepage.html"><i class="fa fa-home" aria-hidden="true"></i>Home</a></li>
<li> <a href="viewfaculty.php">View Faculty</a></li>
<li> <a href="viewcourse.php">View Course</a></li>
<li> <a href="homepage.html"><i class="fa fa-sign-out" aria-hidden="true"></i>Log out</a></li>
<ul>
</div>
</body>


<table class="table table-bordered table-striped table-hover">
    <h1>Faculty</h1><hr>

	<tr>
		<th>NAME</th>
		<th>Email</th>
		<th>Phone number</th>
    <th>Education</th>
    <th>Experience</th>

	</tr>
	<?php
$sql=mysqli_query($conn,"select * from faculty");
while($res=mysqli_fetch_assoc($sql))
{
?>
<tr>

		<td><?php echo $res['name']; ?></td>
		<td><?php echo $res['email']; ?></td>
    	<td><?php echo $res['phone_no']; ?></td>
      	<td><?php echo $res['education']; ?></td>
        	<td><?php echo $res['experience']; ?></td>


	</td>
	</tr>

<?php
}
?>
