<?php
function fetch_data()
{
    $output = '';
    $conn = mysqli_connect("localhost", "root", "", "student");
    $sql = "SELECT * FROM faculty ORDER BY id ASC";
    $result = mysqli_query($conn, $sql);
    while ($row = mysqli_fetch_array($result)) {
        $output .= '<tr>
                          <td>' . $row["id"] . '</td>
                          <td>' . $row["name"] . '</td>
                          <td>' . $row["email"] . '</td>
                          <td>' . $row["address"] . '</td>
                          <td>' . $row["phone_no"] . '</td>
                          <td>' . $row["education"] . '</td>
                          <td>' . $row["experience"] . '</td>

                     </tr>
                          ';
    }
    return $output;
}

    require_once('TCPDF-master/tcpdf.php');
    class MYPDF extends TCPDF{
      public function Footer(){
        $this->SetY(-15);
        $this->SetFont('helvetica','N',10);
        $this->cell(0,10,date("F j,Y,g:i a"),0,false,'C',0,'',0,false,'T','M');
        $this->cell(0,10,'Page'.$this->getAliasNumPage().'/'.$this->getAliasNbPages(),0,false,'C',0,'',0,false,'T','M');

      }
    }
    $obj_pdf = new MYPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
    $obj_pdf->SetCreator(PDF_CREATOR);
    $obj_pdf->SetTitle("USER");
    $obj_pdf->SetHeaderData('', '', PDF_HEADER_TITLE, PDF_HEADER_STRING);
    $obj_pdf->setHeaderFont(array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
    $obj_pdf->setFooterFont(array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
    $obj_pdf->SetDefaultMonospacedFont('helvetica');
    $obj_pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
    $obj_pdf->SetMargins(PDF_MARGIN_LEFT, '10', PDF_MARGIN_RIGHT);
    $obj_pdf->setPrintHeader(false);
    $obj_pdf->setPrintFooter(True);
    $obj_pdf->SetAutoPageBreak(TRUE, 10);
    $obj_pdf->SetFont('helvetica', '', 11);
    $obj_pdf->AddPage();
    $content = '';
    $content .= '
      <h4 align="center" style="font-size:20px">Faculty</h4><br />
      <table border="1" cellspacing="0" cellpadding="3">
           <tr>
                <th width="5%">Id</th>
                <th width="20%">Name</th>
                <th width="20%">Email</th>
                <th width="15%">Address</th>
                <th width="15%">Phone number</th>
                <th width="13%">Education</th>
                <th width="20%">Experience(year)</th>

           </tr>
      ';
    $content .= fetch_data();
    $content .= '</table>';
    $obj_pdf->writeHTML($content);
    $obj_pdf->Output('faculty.pdf', 'I');
