<?php

$dbhost = "localhost";
 $dbuser = "root";
 $dbpass = "";
 $db = "student";


 $conn = new mysqli($dbhost, $dbuser, $dbpass,$db) or die("Connect failed: %s\n". $conn -> error);

?>

<head>
<title>Admin page</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<link rel="stylesheet" href="style3.css">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

</head>
<body style="background-image:url()">
			<div class="menu-bar">
<ul>
<li class="active"> <a href="homepage.html"><i class="fa fa-home" aria-hidden="true"></i>Home</a></li>
<li> <a href="adminreg.html">Admin registration</a></li>
<li> <a href="faculty.html">Faculty</a></li>
<li> <a href="studentTable.php">Update Student</a></li>
<li> <a href="addcourse.html">Course</a></li>
<li> <a href="#">Reports</a>
<div class="sub-menu">
 <ul>
<li> <a href="adminreport.php">Admin</a></li>
<li> <a href="studentreport.php">Student</a></li>
<li> <a href="facultyreport.php">Faculty</a></li>
<li> <a href="coursereport.php">course</a></li>
</ul>
</div>
</li>

<li> <a href="homepage.html">Log out</a></li>
<ul>
</div>
</body>


<script>
	function delProduct(id)
	{
		if(confirm("You want to delete this Student ?"))
		{
		window.location.href='delete_student.php?id='+id;
		}
	}
</script>
<table class="table table-bordered table-striped table-hover">
    <h1>Update</h1><hr>

	<tr>
		<th>ID</th>
		<th>NAME</th>
		<th>User NAme</th>
		<th>DELETE</th>
    <th>UPDATE</th>

	</tr>
	<?php
$i=1;
$sql=mysqli_query($conn,"select * from studentreg");
while($res=mysqli_fetch_assoc($sql))
{
?>
<tr>
        <td><?php echo $res['id']; ?></td>
		<td><?php echo $res['name']; ?></td>
		<td><?php echo $res['username']; ?></td>
		<td><a href="#" onclick="delProduct('<?php echo $res['id']; ?>')"><span  style='color:red'>Delete</span></a></td>
		<td><a href="updatestudent.php?id=<?php echo $res['id'];?>">update</a></td>

	</td>
	</tr>

<?php
}
?>
